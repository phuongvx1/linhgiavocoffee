import React, { Component } from "react";
import { Link } from "react-router-dom";
import $ from "jquery";

const ItemSideBar = ({ url, icon, title }) => {
  return (
    <li className="nav-item">
      <Link className="nav-link collapsed" to={url}>
        <i className={icon}></i>
        <span>{title}</span>
      </Link>
    </li>
  );
};

const MenuBar = () => {
  const adminBar = [
    {
      title: "Gift Code",
      icon: "fa-solid fa-gift",
      url: "/customer",
      btn: "btn-gift-code",
    },
    // {
    //   title: "Product",
    //   icon: "fas fa-images",
    //   url: "/addproduct",
    // },
    // {
    //   title: "Post",
    //   icon: "fa fa-edit",
    //   url: "/post",
    // },
    // {
    //   title: "Doctor",
    //   icon: "fas fa-boxes",
    //   url: "/adddoctor",
    // },
    // {
    //   title: "Feedback",
    //   icon: "fab fa-amazon-pay",
    //   url: "/feedback",
    // },
    // {
    //   title: "Appointment",
    //   icon: "	fa fa-phone",
    //   url: "/appointment",
    // },
    // {
    //   title: "Bill",
    //   icon: "fas fa-shopping-cart	",
    //   url: "/bill",
    // },
  ];

  const doctorBar = [
    {
      title: "Patient",
      icon: "fas fa-images",
      url: "/patients",
    },
    {
      title: "Patient Record",
      icon: "fas fa-user",
      url: "/patientsrecord",
    },
  ];

  return (
    <div>
      <h2 className="sidebar-brand-text mx-3">Coffee Shop</h2>
      <div>
        {adminBar.map((menuItem, index) => {
          return (
            <div key={index}>
              <Link to="/giftcode">
                <i className={menuItem.icon + " menu-link"}>
                  <span>{" "}{menuItem.title}</span>
                </i>
              </Link>
            </div>
          );
        })}
      </div>
      <hr id="admin-hr" />
    </div>
  );
};
export default MenuBar;
