import { BrowserRouter, Routes, Route } from "react-router-dom";
import React, { Component } from "react";
import MenuBar from "./MenuBar";

import axios from "axios";
import TopBar from "./TopBar";
import Footer from "./Footer";
import GiftCode from "./GiftCode";
class Layout extends Component {
  state = {};
  componentDidMount() {}
  render() {
    return (
      <BrowserRouter>
        <MenuBar />
        <Routes>
          <Route path="/" element={<GiftCode />}></Route>
          <Route path="/giftcode" element={<GiftCode />}></Route>
        </Routes>
      </BrowserRouter>
    );
  }
}
export default Layout;
