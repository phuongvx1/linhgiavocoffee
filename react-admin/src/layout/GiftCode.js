import React, { Component } from "react";
import New from "../GiftCode/New";
import Search from "../GiftCode/Search";
import Avaiable from "../GiftCode/Avaiable";
import Used from "../GiftCode/Used";
import axios from "axios";
import { AVAIABLE_GIFT_CODE, VIEW_USED_GIFT_CODE } from "./Server/API";
class GiftCode extends Component {
  state = {
    viewIndex: 1,
    searchResult: {},
    historyResult: [],
    newLoading: false,
    searchLoading: false,
    avaiableCodes: [],
    avaiableCodesLoading: false,
    usedCodes: [],
    usedCodesLoading: false,
  };

  componentDidMount() {
    this.setState({ viewIndex: 1 });
    const self = this;

    axios.post(AVAIABLE_GIFT_CODE).then(function (response) {
      self.setState({ avaiableCodesLoading: false });
      self.setState({ avaiableCodes: response.data });
    });

    axios.post(VIEW_USED_GIFT_CODE).then(function (response) {
      self.setState({ usedCodesLoading: false });
      self.setState({ usedCodes: response.data });
    });
  }
  render() {
    const btnOnClick = (e) => {
      const index = e.target.value;
      this.setState({ viewIndex: index });
      const self = this;

      if (index === 3) {
        self.setState({ avaiableCodesLoading: true });
        axios.post(AVAIABLE_GIFT_CODE).then(function (response) {
          self.setState({ avaiableCodesLoading: false });
          self.setState({ avaiableCodes: response.data });
        });
      }

      if (index === 4) {
        self.setState({ usedCodesLoading: true });
        axios.post(VIEW_USED_GIFT_CODE).then(function (response) {
          self.setState({ usedCodesLoading: false });
          self.setState({ avaiableCodes: response.data });
        });
      }
    };

    return (
      <div>
        <div className="btn-form-group">
          <button
            id={this.state.viewIndex == 1 ? "btn-fade-in" : "btn-fade-out"}
            value={1}
            onClick={btnOnClick}
          >
            New
          </button>
          <button
            id={this.state.viewIndex == 2 ? "btn-fade-in" : "btn-fade-out"}
            value={2}
            onClick={btnOnClick}
          >
            Search
          </button>
          <button
            id={this.state.viewIndex == 3 ? "btn-fade-in" : "btn-fade-out"}
            value={3}
            onClick={btnOnClick}
          >
            Avaiable
          </button>

          <button
            id={this.state.viewIndex == 4 ? "btn-fade-in" : "btn-fade-out"}
            value={4}
            onClick={btnOnClick}
          >
            Used
          </button>
        </div>
        <hr id="admin-hr" />
        <div className="container">
          <div
            className={
              this.state.viewIndex == 1 ? "popup-fade-in" : "popup-fade-out"
            }
          >
            {this.state.appointmentLoading ? (
              <div className="container">
                <div className="row">
                  <div className="d-flex justify-content-center">
                    <div className="spinner-border" role="status">
                      <span className="visually-hidden">Loading...</span>
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              <New />
            )}
          </div>
          <div
            className={
              this.state.viewIndex == 2 ? "popup-fade-in" : "popup-fade-out"
            }
          >
            {this.state.billloading ? (
              <div className="container">
                <div className="row">
                  <div className="d-flex justify-content-center">
                    <div className="spinner-border" role="status">
                      <span className="visually-hidden">Loading...</span>
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              <Search />
            )}
          </div>
          <div
            className={
              this.state.viewIndex == 3 ? "popup-fade-in" : "popup-fade-out"
            }
          >
            {this.state.avaiableCodesLoading ? (
              <div className="container">
                <div className="row">
                  <div className="d-flex justify-content-center">
                    <div className="spinner-border" role="status">
                      <span className="visually-hidden">Loading...</span>
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              <Avaiable
                avaiableCodes={this.state.avaiableCodes}
                avaiableCodesLoading={this.state.avaiableCodesLoading}
              />
            )}
          </div>

          <div
            className={
              this.state.viewIndex == 4 ? "popup-fade-in" : "popup-fade-out"
            }
          >
            {this.state.usedCodesLoading ? (
              <div className="container">
                <div className="row">
                  <div className="d-flex justify-content-center">
                    <div className="spinner-border" role="status">
                      <span className="visually-hidden">Loading...</span>
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              <Used
                usedCodes={this.state.usedCodes}
                usedCodesLoading={this.state.usedCodesLoading}
              />
            )}
          </div>
        </div>
      </div>
    );
  }
}
export default GiftCode;
