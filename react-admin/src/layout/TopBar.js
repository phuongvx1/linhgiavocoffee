// import { Link } from "react-router-dom";
// import React, { Component } from "react";
// import axios from "axios";

// class TopBar extends Component {
//   render() {
//     let admin = {};
//     const isAdminLogin = () => {
//       if (localStorage.getItem("admin_info") == null) {
//         window.location.href = "http://localhost:3001/login";
//       } else {
//         admin = JSON.parse(localStorage.getItem("admin_info"));
//       }
//     };
//     isAdminLogin();

//     const LogOut = () => {
//       const id = admin.id;
//       axios
//         .post(`http://127.0.0.1:8000/api/logout`, { id })
//         .then(function (response) {
//           if (response.data > 0) {
//             localStorage.removeItem("admin_info");
//             window.location.href = "http://localhost:3001/login";
//           }
//         })
//         .catch((err) => {
//           console.log(err);
//         });
//     };
//     return (
//       <nav className="navbar navbar-expand navbar-light bg-white topbar mb-0 static-top shadow">
//         <form className="form-inline">
//           <button
//             id="sidebarToggleTop"
//             className="btn btn-link d-md-none rounded-circle mr-3"
//           >
//             <i className="fa fa-bars"></i>
//           </button>
//         </form>

//         <ul className="navbar-nav ml-auto">
//           <div className="topbar-divider d-none d-sm-block"></div>

//           <li className="nav-item dropdown no-arrow">
//             <Link
//               className="nav-link dropdown-toggle"
//               to="#"
//               id="userDropdown"
//               role="button"
//               data-toggle="dropdown"
//               aria-haspopup="true"
//               aria-expanded="false"
//             >
//               <span className="mr-2 d-none d-lg-inline text-gray-600 small">
//                 Hello {admin.name}
//               </span>
//               <img
//                 className="img-profile rounded-circle"
//                 src={"http://localhost:8000/AdminImage/" + admin.avatar_name}
//               />
//             </Link>
//             <div
//               className="dropdown-menu dropdown-menu-right shadow animated--grow-in"
//               aria-labelledby="userDropdown"
//             >
//               <Link className="dropdown-item" to="/adminprofile">
//                 <i className="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
//                 Profile
//               </Link>
//               <div className="dropdown-divider"></div>
//               <button className="dropdown-item" onClick={LogOut}>
//                 <i className="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
//                 Logout
//               </button>
//             </div>
//           </li>
//         </ul>
//       </nav>
//     );
//   }
// }

// export default TopBar;
