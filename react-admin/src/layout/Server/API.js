const API = "http://powerful-scrubland-79447.herokuapp.com/api/";

export const ADD_NEW_GIFT_CODE = API + "addnewgiftcode";
export const AVAIABLE_GIFT_CODE = API + "viewallgiftcode";
export const USE_GIFT_CODE = API + "usegiftcode";
export const SEARCH_GIFT_CODE = API + "searchgiftcode";
export const VIEW_USED_GIFT_CODE = API + "viewusedgiftcodes";
export const DELETE_GIFT_CODE = API + "deletegiftcode";
