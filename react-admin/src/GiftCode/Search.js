import React, { Component } from "react";
import $ from "jquery";
import axios from "axios";
import { Link } from "react-router-dom";
import { SEARCH_GIFT_CODE, USE_GIFT_CODE } from "../layout/Server/API";

class Search extends Component {
  state = {
    loading: false,
    searchResult: [],
    currentPage: 1,
  };

  render() {
    let i = 1;

    if (this.state.currentPage > 1) {
      i = 1;
    }
    const productsPerPage = 10;

    const indexOfLastPage = this.state.currentPage * productsPerPage;
    const indexOfFirstPage = indexOfLastPage - productsPerPage;
    const currentProducts = this.state.searchResult.slice(
      indexOfFirstPage,
      indexOfLastPage
    );

    let pageNumbers = [];
    for (
      let i = 1;
      i <= Math.ceil(this.state.searchResult.length / productsPerPage);
      i++
    ) {
      pageNumbers.push(i);
    }

    const paginate = (number) => {
      this.setState({ currentPage: number });
    };

    const onStatusChange = (e) => {
      const gift_code_id = e.target.value;
      axios.post(USE_GIFT_CODE, { gift_code_id }).then(function (response) {
        if (response.data.gift_code_status <= 0) {
          alert("Use gift code successfull.");
        } else {
          alert("Use gift code fail.");
        }
      });
    };

    const onSearch = () => {
      const gift_code_barcode = $("#content").val();
      this.setState({ loading: true });
      const self = this;
      axios
        .post(SEARCH_GIFT_CODE, { gift_code_barcode })
        .then(function (response) {
          if (response.data == 1) {
            self.setState({ searchResult: [] });
            $("#search-result").text("Can not found gift code!");
            $("#search-result").css("color", "red");
          } else {
            self.setState({ searchResult: response.data });
          }
          self.setState({ loading: false });
          setInterval(clearText, 10000);
        });
    };

    const clearText = () => {
      $("#search-result").text(" ");
      $("#content").val(" ");
    };

    return (
      <div className="container">
        <div className="row">
          <div className="col-1"></div>
          <div className="col-3">
            <div className="new-gift-code">
              <label>Search</label>
              <input
                id="content"
                placeholder="gift code or phone number"
                className="form-control"
              />

              <div
                id="search-result"
                className="small font-italic form-waring-text"
              ></div>
            </div>
            <br />
            <button
              className="btn-new-gift-code"
              id="btn-new-gift-code"
              onClick={onSearch}
            >
              <b>SEARCH</b>
            </button>
            {this.state.loading ? (
              <div
                className="spinner-border text-dark"
                id="register-loading-ring"
                role="status"
              >
                <span className="visually-hidden">Loading...</span>
              </div>
            ) : null}
            <br />
            <br />
          </div>
          {this.state.searchResult.length <= 0 ? (
            <div className="container">
              <div className="row">
                <div className="col-2"></div>
                <div className="col-8">
                  <span>No record found</span>
                </div>
              </div>
            </div>
          ) : (
            <div className="table-responsive">
              <table
                className="table table-bordered"
                id="dataTable"
                style={{ width: "100%" }}
                cellSpacing="0"
              >
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Contact</th>
                    <th>Gift Code</th>
                    <th>Discount</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody id="bill-records">
                  {currentProducts.map((result, index) => {
                    return (
                      <tr key={index}>
                        <td>{(this.state.currentPage - 1) * 10 + i++}</td>
                        <td>{result.customer_name}</td>
                        <td>{result.customer_contact}</td>
                        <td>{result.gift_code_barcode}</td>
                        <td>{result.customer_discount}</td>
                        <td>
                          {result.gift_code_status == 0 ? "Used" : "Avaiable"}
                        </td>
                        <td>
                          {result.gift_code_status == 0 ? (
                            <label className="switch">
                              <input type="checkbox" disabled />
                              <span className="slider round"></span>
                            </label>
                          ) : (
                            <label className="switch">
                              <input
                                type="checkbox"
                                defaultChecked
                                onChange={onStatusChange}
                                value={result.gift_code_id}
                              />
                              <span className="slider round"></span>
                            </label>
                          )}
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          )}
        </div>
        <div className="col-md-10" style={{ padding: "0px", margin: "0px" }}>
          <ul className="pagination">
            {pageNumbers.map((number, index) => (
              <li key={index} className="page-item">
                <Link
                  to="#"
                  className="page-link"
                  onClick={() => paginate(number)}
                >
                  {number}
                </Link>
              </li>
            ))}
          </ul>
        </div>
      </div>
    );
  }
}
export default Search;
