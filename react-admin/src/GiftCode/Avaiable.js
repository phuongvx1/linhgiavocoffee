import React, { Component } from "react";
import $ from "jquery";
import axios from "axios";
import { Link } from "react-router-dom";
import { DELETE_GIFT_CODE, USE_GIFT_CODE } from "../layout/Server/API";

class Avaiable extends Component {
  state = {
    loading: false,
    history: [],
    currentPage: 1,
  };
  componentDidMount() {
    this.setState({ currentPage: 1 });
  }
  render() {
    const { avaiableCodes, avaiableCodesLoading } = this.props;

    function Search() {
      var value = $("#search").val().toLowerCase();
      $("#bill-records tr").filter(function () {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
      });
    }
    let i = 1;

    if (this.state.currentPage > 1) {
      i = 1;
    }
    const productsPerPage = 10;

    const indexOfLastPage = this.state.currentPage * productsPerPage;
    const indexOfFirstPage = indexOfLastPage - productsPerPage;
    const currentProducts = avaiableCodes.slice(
      indexOfFirstPage,
      indexOfLastPage
    );
    let pageNumbers = [];
    for (
      let i = 1;
      i <= Math.ceil(avaiableCodes.length / productsPerPage);
      i++
    ) {
      pageNumbers.push(i);
    }

    const paginate = (number) => {
      this.setState({ currentPage: number });
    };

    if (currentProducts.length <= 0) {
      if (this.state.loading) {
        return (
          <div
            className="spinner-border text-dark"
            id="register-loading-ring"
            role="status"
          >
            <span className="visually-hidden">Loading...</span>
          </div>
        );
      } else {
        return (
          <div className="container">
            <div className="row">
              <div className="col-5"></div>
              <div className="col-2">
                <span>No record found</span>
              </div>
              <div className="col-5"></div>
            </div>
          </div>
        );
      }
    }
    const onStatusChange = (e) => {
      const gift_code_id = e.target.value;
      axios.post(USE_GIFT_CODE, { gift_code_id }).then(function (response) {
        if (response.data > 0) {
          alert("Use gift code successfull.");
        } else {
          alert("Use gift code fail.");
        }
      });
    };

    const onDeleteGiftCode = (e) => {
      const gift_code_id = e.target.value;
      axios.post(DELETE_GIFT_CODE, { gift_code_id }).then(function (response) {
        if (response.data > 0) {
          alert("Delete gift code successfull.");
        } else {
          alert("Delete gift code fail.");
        }
      });
    };

    return (
      <div className="container">
        <div className="card shadow mb-4">
          <div className="card-header py-3">
            <h4 className="m-0 font-weight-bold">History</h4>
          </div>
          <div className="card-body">
            <input
              className="form-control col-3 mb-3"
              id="search"
              type="text"
              onKeyUp={Search}
              placeholder="Search.."
            />
            {avaiableCodesLoading ? (
              <div
                className="spinner-border text-dark"
                id="register-loading-ring"
                role="status"
              >
                <span className="visually-hidden">Loading...</span>
              </div>
            ) : (
              <div className="table-responsive">
                <table
                  className="table table-bordered"
                  id="dataTable"
                  style={{ width: "100%" }}
                  cellSpacing="0"
                >
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Name</th>
                      <th>Contact</th>
                      <th>Gift Code</th>
                      <th>Discount</th>
                      <th>Status</th>
                      <th>Use</th>
                      <th>Delete</th>
                    </tr>
                  </thead>
                  <tbody id="bill-records">
                    {currentProducts.map((bh, index) => {
                      return (
                        <tr key={index}>
                          <td>{(this.state.currentPage - 1) * 10 + i++}</td>
                          <td>{bh.customer_name}</td>
                          <td>{bh.customer_contact}</td>
                          <td>{bh.gift_code_barcode}</td>
                          <td>{bh.customer_discount}</td>
                          <td>
                            {bh.gift_code_status == 0 ? "Used" : "Avaiable"}
                          </td>
                          <td>
                            {bh.gift_code_status == 0 ? (
                              <label className="switch">
                                <input type="checkbox" disabled />
                                <span className="slider round"></span>
                              </label>
                            ) : (
                              <label className="switch">
                                <input
                                  type="checkbox"
                                  defaultChecked
                                  onChange={onStatusChange}
                                  value={bh.gift_code_id}
                                />
                                <span className="slider round"></span>
                              </label>
                            )}
                          </td>
                          <button
                            id="btn-delete-gift-code"
                            value={bh.gift_code_id}
                            onClick={onDeleteGiftCode}
                          >
                            Delete
                          </button>
                          <td></td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            )}

            <div
              className="col-md-10"
              style={{ padding: "0px", margin: "0px" }}
            >
              <ul className="pagination">
                {pageNumbers.map((number, index) => (
                  <li key={index} className="page-item">
                    <Link
                      to="#"
                      className="page-link"
                      onClick={() => paginate(number)}
                    >
                      {number}
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Avaiable;
