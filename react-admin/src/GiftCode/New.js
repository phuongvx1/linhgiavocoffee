import React, { Component } from "react";
import $ from "jquery";
import axios from "axios";
import { ADD_NEW_GIFT_CODE } from "../layout/Server/API";

class New extends Component {
  state = {
    loading: false,
  };
  render() {
    const btnNewGiftCode = () => {
      const customer_name = $("#customer_name").val();
      const customer_contact = $("#customer_contact").val();
      const customer_discount = $("#select-discount").val();

      this.setState({ loading: true });
      const self = this;
      axios
        .post(ADD_NEW_GIFT_CODE, {
          customer_name,
          customer_contact,
          customer_discount,
        })
        .then(function (response) {
          if (response.data > 0) {
            $("#customer_name").val("");
            $("#firstnameResult").text("");

            $("#customer_name").val("");
            $("#lastnameResult").text("");

            self.setState({ loading: false });

            $("#add-new-result").text("Successfully");
            $("#add-new-result").css("color", "green");
          }
          setInterval(clearText, 2000);
        });
    };

    const clearText = () => {
      $("#add-new-result").text(" ");
    };
    return (
      <div className="container">
        <div className="row">
          <div className="col-1"></div>
          <div className="col-3">
            <div className="new-gift-code">
              <label>Name</label>
              <input
                id="customer_name"
                placeholder="example: Linh Gia Vo"
                className="form-control"
                //   onBlur={onBirthdayOnBlur}
              />
              <div
                id="birthdayResult"
                className="small font-italic form-waring-text"
              ></div>
            </div>
            <div className="new-gift-code">
              <label>Mobile Number</label>
              <input
                type="text"
                id="customer_contact"
                placeholder="example: 0355487651"
                className="form-control"
                //   onBlur={onPhoneNumberBlur}
              />
              <div
                id="phonenumberResult"
                className="small font-italic form-waring-text"
              ></div>
            </div>

            <div className="new-gift-code">
              <label>Discount</label>
              <select
                defaultValue="50"
                className="form-control"
                id="select-discount"
              >
                <option value="50">50%</option>
                <option value="100">100%</option>
              </select>
            </div>

            <br />
            <button
              className="btn-new-gift-code"
              id="btn-new-gift-code"
              onClick={btnNewGiftCode}
            >
              <b>CONFIRM</b>
            </button>
            {this.state.loading ? (
              <div
                className="spinner-border text-dark"
                id="register-loading-ring"
                role="status"
              >
                <span className="visually-hidden">Loading...</span>
              </div>
            ) : null}
            <div
              id="add-new-result"
              className="small font-italic form-waring-text"
            ></div>
          </div>
        </div>
      </div>
    );
  }
}
export default New;
