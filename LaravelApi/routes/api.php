<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GiftCodeController;
use App\Models\GiftCode;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/addnewgiftcode',[GiftCodeController::class, 'AddNewGiftCode']);
Route::post('/viewallgiftcode',[GiftCodeController::class, 'ViewAllGiftCode']);
Route::post('/usegiftcode',[GiftCodeController::class, 'UseGiftCode']);
Route::post('/searchgiftcode',[GiftCodeController::class, 'SearchGiftCode']);
Route::post('/viewusedgiftcodes',[GiftCodeController::class, 'ViewUsedGiftCodes']);
Route::post('/deletegiftcode',[GiftCodeController::class, 'DeleteGiftCode']);

