<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GiftCode extends Model
{
    use HasFactory;
    protected $table = 'GiftCode';
    protected $primaryKey = 'gift_code_id';
    public $timestamps = false;
    protected $fillable = [
        'gift_code_id',
        'gift_code_barcode',
        'customer_name',
        'customer_contact',
        'customer_discount',
        'gift_code_status',
    ];
}
