<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GiftCode;
use Str;
use Illuminate\Support\Facades\DB;

class GiftCodeController extends Controller
{
    public function AddNewGiftCode(Request $request){

        $barcode = '';
        $exists = 0;
        do
        {
            $barcode = strtoupper(Str::random(8));
            $exists = DB::select("select * from GiftCode where gift_code_barcode like '".$barcode."'");
        }while(count($exists) != 0);

        DB::select("insert into GiftCode(customer_name,customer_contact,customer_discount,gift_code_barcode)
                     values (?, ?,?,?)", 
                     [$request->customer_name, $request->customer_contact,$request->customer_discount,$barcode]);
        return 1;
    }

    public function ViewAllGiftCode(){
        $tmp = DB::select("select * from GiftCode where gift_code_status = 1 order by gift_code_id desc");
        return $tmp;
    }

    public function ViewUsedGiftCodes(){
        $tmp = DB::select("select * from GiftCode where gift_code_status = 0 order by gift_code_id desc");
        return $tmp;
    }

    public function DeleteGiftCode(Request $request){
        $id = $request->gift_code_id;
        DB::select("delete from GiftCode where gift_code_id = ".$id);
        $exists = DB::select("select * from GiftCode where gift_code_id = ".$id);

        if(count($exists) <= 0)
        {
            return 1;
        }
        return 0;
    }

    public function UseGiftCode(Request $request){
        $id = $request->gift_code_id;
        $use_time = time();
        DB::select("update GiftCode set gift_code_status = 0, use_time = '".$use_time."' where gift_code_id = ".$id);
        $tmp = DB::select("select gift_code_status from GiftCode where gift_code_id = ".$id." and gift_code_status = 0");

        return count($tmp);
    }

    public function SearchGiftCode(Request $request){
        $content = $request->gift_code_barcode;

        $tmp = DB::select("Select * from GiftCode where gift_code_barcode like '"
        .$content."' or customer_contact = '"
        .$content."' order by gift_code_id desc");
        if(count($tmp) > 0)
        {
            return $tmp;
        }
        return 1;
    }
}
